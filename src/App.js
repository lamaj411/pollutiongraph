import "./App.css";
import React from "react";
import axios from "axios";
import { Line, defaults } from "react-chartjs-2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import _ from "lodash";
const App = () => {
  //states
  const [data, setData] = React.useState(null);
  const [startDate, setStartDate] = React.useState(new Date());

  //setting graph animation false
  defaults.animation = false;

  //fetching data for first time
  React.useEffect(() => {
    getPollutionDataByDate(startDate);
  }, []);

  //function for fetching pollution data
  const getPollutionDataByDate = (date) => {
    axios
      .get(
        `https://u50g7n0cbj.execute-api.us-east-1.amazonaws.com/v1/measurements?date_from=${date.setHours(
          0,
          0,
          0,
          0
        )}&date_to=${date.setHours(
          23,
          59,
          59,
          999
        )}&limit=100&page=1&offset=0&sort=desc&radius=1000&country_id=IN&city=Delhi&location_id=11607&order_by=datetime`
      )
      .then(async (response) => {
        console.log(response, "response");
        // handle success
        if (response.status === 200) {
          console.log("200");
          let labels = [];
          let data1 = [];
          _.sortBy(
            _.uniqBy(response.data.results, "parameter"),
            "parameter"
          ).map(async (item) => {
            await labels.push(item.parameter);
            await data1.push(item.value);
          });
          console.log(labels, data1);
          let dataTemp = await {
            labels: labels,
            datasets: [
              {
                label: "Air pollution rate - Delhi",
                fill: true,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: data1,
              },
            ],
          };
          await setData(dataTemp);
        }
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  };

  //updating the graph when date changes
  React.useLayoutEffect(() => {
    getPollutionDataByDate(startDate);
  }, [startDate]);

  return (
    <div className="Container">
      <div style={{ paddingBottom: 30 }}>
        <div className="InputRow">
          <div className="InputLabel">Date :</div>
          <div>
            <DatePicker
              selected={startDate}
              onChange={async (date) => {
                await setStartDate(date);
              }}
            />
          </div>
        </div>
      </div>
      <div className="GraphContainer">
        <Line data={data} redraw={false} width={600} height={400} />
      </div>
    </div>
  );
};

export default App;
